function say (num)
{
    if (num % 15 == 0)
        return "Fizz Buzz"
    else if (num % 5 == 0)
        return "Buzz"
    if (num % 3 == 0)
        return "Fizz"
    else return num.toString()
}

module.exports = say;